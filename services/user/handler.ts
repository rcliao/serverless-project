import { APIGatewayProxyHandler } from 'aws-lambda';
import { MemoryUserDAO, User }  from '../../src/user/user';
import 'source-map-support/register';

export const hello: APIGatewayProxyHandler = async (event, _context) => {
  const user: User = new MemoryUserDAO();
  user.register('eric', 'password', {firstName: 'eric', lastName: 'liao', email: 'eric.liao@allergan.com', phone: '1234567890'});
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
      input: event,
    }),
  };
}
