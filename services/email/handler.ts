import { APIGatewayProxyHandler } from "aws-lambda";
import 'source-map-support/register';

import { Mailer, MockMailer } from '../../src/email/mailer';

export const hello: APIGatewayProxyHandler = async (event, _context) => {
  const mailer: Mailer = new MockMailer();
  mailer.send('from@test.com', 'to@there.com', 'this is a subject', '<p>that is a <b>content</b><p>');
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
      input: event,
    }),
  };
}
