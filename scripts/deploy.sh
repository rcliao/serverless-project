#!/bin/bash

dir="$(pwd)"

cd "$dir/services/user" && npm i && sls deploy -v
cd "$dir/services/email" && npm i && sls deploy -v
