export interface Mailer {
  send (from: string, to: string, subject: string, content: string): void;
};

export class MockMailer implements Mailer {
  send (from: string, to: string, subject: string, content: string): void {
    console.log('sending email', {from, to, subject, content});
  }
};
