export interface Profile {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}

export interface User {
  register(username: string, password: string, profile: Profile): void;
  login(username: string, password: string): void;
}

export class MemoryUserDAO implements User {
  register(username: string, password: string, profile: Profile) {
    console.log('register', username, password, profile);
  }

  login(username: string, password: string) {
    console.log('login', username, password);
  }
}
